var should = require('chai').should();
var request = require('supertest');

var app = require('../app');
var sampleRequest = require('../samples/request.json');
var sampleResponse = require('../samples/response.json');

describe('POST /', function() {

  it('returns 400 when payload key is missing', function(done) {
    request(app)
      .post('/')
      .send({'blah': 'blah'})
      .expect(400, done);
  });

  it('return filtered shows when a valid request is posted', function(done) {
    request(app)
      .post('/')
      .send(sampleRequest)
      .expect(200)
      .end(function(err, res) {
        // Asserts that the target is deeply equal to value.
        res.body.should.eql(sampleResponse);
        done();
      });
  });

});