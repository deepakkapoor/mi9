var _ = require('lodash');

module.exports.filter = function(req, res) {
  // handle empty request and request without payload
  if (!req.body || !req.body.payload) {
    res.status(400).json({
      "error": "Could not decode request: JSON parsing failed"
    });
    return;
  }

  var filteredShows = [];

  // love lodash
  _.filter(req.body.payload, function(show) {
    return show.episodeCount > 0 && show.drm;
  }).forEach(function(show) {
    filteredShows.push({
      image: show.image.showImage,
      slug: show.slug,
      title: show.title
    });

  });

  res.json({
    response: filteredShows
  });

};
