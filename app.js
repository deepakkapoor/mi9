var express     = require('express');
var bodyParser  = require('body-parser');
var shows       = require('./shows');

var app = express();
var port = process.env.PORT || 8000;

app.use(bodyParser.json());


app.use(function(req, res, next) {
  res.set({
     'Developed-By': 'Deepak Kapoor'
   });
  next();
});

// handle invalid JSON 
app.use(function(err, req, res, next) {
  if(err instanceof SyntaxError|| err.message.indexOf('json') > -1) {
    res.status(400).json({ "error": "Could not decode request: JSON parsing failed" });
  }
  next(err);
});

app.listen(port);
module.exports = app;

app.route('/').post(shows.filter);

console.log('Mi9 application started on port:' + port);